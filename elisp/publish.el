;;; publish.el --- Publish reveal.js presentations from Org files
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; SPDX-FileCopyrightText: 2019-2023 Jens Lechtenbörger
;; SPDX-License-Identifier: GPL-3.0-or-later

;;; License: GPL-3.0-or-later

;;; Commentary:
;; Publication of Org source files to reveal.js uses Org export
;; functionality offered by org-re-reveal and oer-reveal.
;; Initialization code for both is provided by emacs-reveal.
;; Note that org-re-reveal and oer-reveal are available on MELPA.
;;
;; Use this file from its parent directory with the following shell
;; command:
;; emacs --batch --load elisp/publish.el

;;; Code:
;; Avoid update of emacs-reveal, enable stacktraces, disable delays.
(setq emacs-reveal-managed-install-p nil
      debug-on-error t
      oer-reveal-warning-delay nil)

;; Setup dot to generate images.
(setq oer-reveal-publish-babel-languages '((dot . t) (emacs-lisp . t)))

;; Set up load-path.
(let ((install-dir
       (mapconcat #'file-name-as-directory
                  `(,user-emacs-directory "elpa" "emacs-reveal") "")))
  (add-to-list 'load-path install-dir)
  (condition-case nil
      ;; Either require package with above hard-coded location
      ;; (e.g., in docker) ...
      (require 'emacs-reveal)
    (error
     ;; ... or look for sibling "emacs-reveal".
     (add-to-list
      'load-path
      (expand-file-name "../../emacs-reveal/" (file-name-directory load-file-name)))
     (require 'emacs-reveal))))

;; Use local klipse-libs for offline use.
(add-to-list 'oer-reveal-plugins "klipse-libs")

;; Reduce max-height and font-size for code.
(setq org-re-reveal-klipse-extra-css "<style>
/* Position computations of klipse get confused by reveal.js's scaling.
   Hence, scaling should be disabled with this code.  Fix height of code area
   with scrollbar (use overflow instead of overflow-y to restore CodeMirror
   setting afterwards): */
.reveal section pre { max-height: 60vh; height: 60vh; overflow: auto; }
/* Reset some reveal.js and oer-reveal settings: */
.reveal section pre .CodeMirror pre { font-size: 1em; box-shadow: none; width: auto; padding: 0.1em; display: block; height: auto; overflow: visible; }
.reveal .klipse-result h1, .reveal .klipse-result h2, .reveal .klipse-result h3, .reveal .klipse-result p, .reveal .klipse-result li { margin: 0; }
/* Enlarge cursor: */
.CodeMirror-cursor { border-left: 3px solid black; }
</style>\n")

;; Generate multiplex client presentations if name contains "multiplex".
(setq org-re-reveal-client-multiplex-filter "multiplex")
(add-to-list 'oer-reveal-publish-org-publishing-functions
             #'oer-reveal-publish-to-reveal-client)

;; Publish Org files.
(oer-reveal-publish-all
 (list
  (list "local-css"
	:base-directory "."
	:include '("local.css")
	:exclude ".*"
	:publishing-function 'org-publish-attachment
	:publishing-directory "./public")
  (list "figures"
	:base-directory "figures"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/figures"
	:publishing-function 'org-publish-attachment)
  (list "images"
	:base-directory "img"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/img"
	:publishing-function 'org-publish-attachment)
  (list "socket.io"
        :base-directory "js/socket.io"
        :base-extension "js"
        :publishing-directory "./public/reveal.js/plugin/multiplex"
        :publishing-function 'org-publish-attachment)
  (list "prog-audio"
	:base-directory "programming/audio"
	:base-extension (regexp-opt '("ogg" "mp3"))
	:publishing-directory "./public/audio"
	:publishing-function 'org-publish-attachment)
  (list "dm-audio"
	:base-directory "data-management/audio"
	:base-extension (regexp-opt '("ogg" "mp3"))
	:publishing-directory "./public/audio"
	:publishing-function 'org-publish-attachment)
  (list "html"
	:base-directory "programming/html"
	:base-extension (regexp-opt '("html"))
	:publishing-directory "./public/html"
	:publishing-function 'org-publish-attachment)
  (list "dm-texts"
       	:base-directory "data-management/texts"
       	:base-extension "org"
        :html-postamble ""
       	:publishing-function '(oer-reveal-publish-to-pdf oer-reveal-publish-to-html)
       	:publishing-directory "./public/texts")
  (list "dm-imgs"
        :base-directory "data-management/texts"
        :base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
        :publishing-function 'org-publish-attachment
        :publishing-directory "./public/texts")
  (list "quizzes"
	:base-directory "programming/quizzes"
	:base-extension (regexp-opt '("js"))
	:publishing-directory "./public/quizzes"
	:publishing-function 'org-publish-attachment)
  (list "title-logos"
	:base-directory "non-free-logos/title-slide"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/title-slide"
	:publishing-function 'org-publish-attachment)
  (list "theme-logos"
	:base-directory "non-free-logos/reveal-css"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/reveal.js/dist/theme"
	:publishing-function 'org-publish-attachment)))
;;; publish.el ends here
